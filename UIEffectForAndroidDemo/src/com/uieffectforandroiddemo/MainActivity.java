package com.uieffectforandroiddemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.uieffectforandroid.activityanim.AnimatedRectLayout;
import com.uieffectforandroid.dialog.Effectstype;
import com.uieffectforandroid.dialog.NiftyDialogBuilder;

public class MainActivity extends Activity implements OnClickListener {
	private int mAnimationType = AnimatedRectLayout.ANIMATION_RANDOM;
	private Button dialog1, dialog2, dialog3, dialog4, dialog5,dialog6;
	/**
	 * 需要自定义，继承此类即可
	 */
	NiftyDialogBuilder niftyDialogBuilder;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		niftyDialogBuilder = new NiftyDialogBuilder(this);
		dialog1 = (Button) findViewById(R.id.dialog1);
		dialog2 = (Button) findViewById(R.id.dialog2);
		dialog3 = (Button) findViewById(R.id.dialog3);
		dialog4 = (Button) findViewById(R.id.dialog4);
		dialog5 = (Button) findViewById(R.id.dialog5);
		dialog6 = (Button) findViewById(R.id.dialog6);
		dialog1.setOnClickListener(this);
		dialog2.setOnClickListener(this);
		dialog3.setOnClickListener(this);
		dialog4.setOnClickListener(this);
		dialog5.setOnClickListener(this);
		dialog6.setOnClickListener(this);
	}
@Override
protected void onPause() {
	// TODO Auto-generated method stub
	super.onPause();
}
@Override
protected void onResume() {
	// TODO Auto-generated method stub
	super.onResume();
}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.dialog1:
			niftyDialogBuilder.withEffect(Effectstype.Fadein);
			niftyDialogBuilder.show();
			break;
		case R.id.dialog2:
			niftyDialogBuilder.withEffect(Effectstype.Fall);
			niftyDialogBuilder.show();
			break;
		case R.id.dialog3:
			niftyDialogBuilder.withEffect(Effectstype.Fliph);
			niftyDialogBuilder.show();
			break;
		case R.id.dialog4:
			niftyDialogBuilder.withEffect(Effectstype.RotateLeft);
			niftyDialogBuilder.show();
			break;
		case R.id.dialog5:
			niftyDialogBuilder.withEffect(Effectstype.Slit);
			niftyDialogBuilder.show();
			break;
		case R.id.dialog6:
			Intent intent = new Intent(this, ActivityAnim.class);
            intent.putExtra("animation_type", mAnimationType);
            startActivity(intent);
            overridePendingTransition(0, 0);
			break;

		default:
			break;
		}
	}

}
