package com.uieffectforandroid.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.uieffectforandroid.R;

/**
 * Created by lee on 2014/7/30.
 */
public class NiftyDialogBuilder extends Dialog implements DialogInterface {

	private Effectstype type = null;

	private int mDuration = -1;

	private View view;

	public NiftyDialogBuilder(Context context) {
		super(context);
		init(context);

	}

	public NiftyDialogBuilder(Context context, int theme) {
		super(context, theme);
		init(context);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		WindowManager.LayoutParams params = getWindow().getAttributes();
		params.height = ViewGroup.LayoutParams.MATCH_PARENT;
		params.width = ViewGroup.LayoutParams.MATCH_PARENT;
		getWindow().setAttributes((WindowManager.LayoutParams) params);

	}

	private void init(Context context) {
		this.setOnShowListener(new OnShowListener() {
			@Override
			public void onShow(DialogInterface dialogInterface) {
				if (type == null) {
					type = Effectstype.Slidetop;
				}
				start(type, view);

			}
		});
	}

	public NiftyDialogBuilder withDuration(int duration) {
		this.mDuration = duration;
		return this;
	}

	public NiftyDialogBuilder withEffect(Effectstype type) {
		this.type = type;
		return this;
	}

	@Override
	public void show() {
		super.show();
	}

	private void start(Effectstype type, View view) {
		BaseEffects animator = type.getAnimator();
		if (mDuration != -1) {
			animator.setDuration(Math.abs(mDuration));
		}
		if (view == null) {
			view =(View)getWindow().getDecorView();
		}
		animator.start(view);
	}

}
