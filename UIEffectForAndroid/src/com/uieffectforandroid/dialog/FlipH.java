package com.uieffectforandroid.dialog;

import android.view.View;

import com.uieffectforandroid.dialog.animaion.ObjectAnimator;

/**
 * Created by lee on 2014/7/31.
 */
public class FlipH  extends BaseEffects{

    @Override
    protected void setupAnimation(View view) {
        getAnimatorSet().playTogether(
                ObjectAnimator.ofFloat(view, "rotationY", -90, 0).setDuration(mDuration)

        );
    }
}
